## About it

This is an Open RPG Sound Library for RPG players! This repo is for version control only. Feel free to contribute, fork this project and use it however you want. But keep in mind that this is under the GNU GPL 3 license!

The idea of this project is to create a tool to RPG masters to control the soundtrack of their adventures in an easy way.

Any contribution to this project would be very much appreciated.

If you're not a developer and you want to use this program that's still under development send me an email and I'll help you to get a running version of this in your computer! :)

openrpgsl being used as a sound manager system for a RPG Master:

![rpgsl-example](/images/rpgsl-example.png)

openrpgsl waiting to be used:
![rpgsl-example-initial](/images/rpgsl-example-initial.jpeg)
